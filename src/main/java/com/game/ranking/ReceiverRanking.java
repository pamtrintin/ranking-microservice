package com.game.ranking;

import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;
import com.game.ranking.models.Ranking;
import com.game.ranking.repositories.RankingRepository;

@Component
public class ReceiverRanking {

	@Autowired
	RankingRepository rankingRepository;
	
	@JmsListener(destination="a.queue.ranking")
	public void receberRanking(HashMap<String, String> ranking) {
		
		Ranking rankingBody = new Ranking();
		
		rankingBody.setNomeJogador(ranking.get("playerName"));
		
		if(ranking.get("gameId") == null){
			rankingBody.setNomeJogo("Jogo sem nome");
		}else {
			rankingBody.setNomeJogo(ranking.get("gameId"));
		}
		
		rankingBody.setPontuacao(Integer.parseInt(ranking.get("total")));
		
	    rankingRepository.save(rankingBody);
				
	}
}
